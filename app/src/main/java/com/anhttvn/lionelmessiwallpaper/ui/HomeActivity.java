package com.anhttvn.lionelmessiwallpaper.ui;

import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.anhttvn.lionelmessiwallpaper.Main;
import com.anhttvn.lionelmessiwallpaper.R;
import com.anhttvn.lionelmessiwallpaper.databinding.ActivityHomeBinding;
import com.anhttvn.lionelmessiwallpaper.model.Banner;
import com.anhttvn.lionelmessiwallpaper.util.BaseActivity;
import com.anhttvn.lionelmessiwallpaper.util.Config;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity  extends BaseActivity {
  private ActivityHomeBinding homeBinding;
  private DatabaseReference mDatabase;

  @Override
  public void init() {
    mDatabase = FirebaseDatabase.getInstance().getReference();
    homeBinding.btnGo.setVisibility(View.GONE);
    if (isConnected()) {
      getBanner();
    } else {
      homeBinding.btnGo.setVisibility(View.VISIBLE);
      homeBinding.imgBanner.setImageResource(R.drawable.banner);
    }

    isBannerADS(homeBinding.adsHome);
    clickLestGo();
  }

  @Override
  public View contentView() {
    homeBinding = ActivityHomeBinding.inflate(getLayoutInflater());
    return homeBinding.getRoot();
  }

  private void clickLestGo() {
    homeBinding.btnGo.setOnClickListener(view -> {
      Intent intent = new Intent(this, Main.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|
              Intent.FLAG_ACTIVITY_CLEAR_TASK |
              Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(intent);
      overridePendingTransition(R.anim.fadein,R.anim.fadeout);
    });
  }

  private void banner(List<Banner> banners) {
    if (banners.size() > 0) {
      CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(this);
      circularProgressDrawable.setStrokeWidth(5f);
      circularProgressDrawable.setCenterRadius(30f);
      circularProgressDrawable.start();
      Banner banner = banners.get(banners.size() - 1);
      Picasso.with(this).load(banner.getPath())
              .placeholder(circularProgressDrawable)
              .error(R.drawable.ic_no_thumbnail)
              .into(homeBinding.imgBanner);
    } else {
      homeBinding.imgBanner.setImageResource(R.drawable.banner);
    }
  }

  private void getBanner() {
    List<Banner> banners = new ArrayList<>();
    DatabaseReference ref2 =  mDatabase.child(Config.BANNER);
    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
          Banner data = dsp.getValue(Banner.class);
          banners.add(data);
        }
        banner(banners);
        homeBinding.btnGo.setVisibility(View.VISIBLE);

      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
        homeBinding.imgBanner.setImageResource(R.drawable.banner);
        homeBinding.btnGo.setVisibility(View.VISIBLE);
      }
    });
  }


}
