package com.anhttvn.lionelmessiwallpaper.ui.home;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.anhttvn.lionelmessiwallpaper.R;
import com.anhttvn.lionelmessiwallpaper.adapter.PhotoAdapter;
import com.anhttvn.lionelmessiwallpaper.databinding.FragmentHomeBinding;
import com.anhttvn.lionelmessiwallpaper.layout.SetWallpaper;
import com.anhttvn.lionelmessiwallpaper.model.Wallpaper;
import com.anhttvn.lionelmessiwallpaper.util.BaseFragment;
import com.anhttvn.lionelmessiwallpaper.util.Config;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 * @author anhtt61
 * @version 1.1.3
 * @2022
 */

public class HomeFragment extends BaseFragment implements PhotoAdapter.OnclickImage{

  private FragmentHomeBinding homeBinding;
  private List<Wallpaper> listImage = new ArrayList<>();
  private DatabaseReference mDatabase;

  private PhotoAdapter mPhotoAdapter;
  @Override
  protected View initView(LayoutInflater inflater, ViewGroup container, boolean b) {
    homeBinding = FragmentHomeBinding.inflate(inflater, container, b);
    return homeBinding.getRoot();
  }

  @Override
  protected void init() {
    mDatabase = FirebaseDatabase.getInstance().getReference();
    homeBinding.wallpapers.setVisibility(View.GONE);
    homeBinding.noData.getRoot().setVisibility(View.GONE);
    if (isConnected()) {
      loadData();
    } else {
      listImage = db.listWallpaper();
      if (listImage.size() > 0) {
        adapter(listImage);
      }
    }

  }

  private void adapter(List<Wallpaper> wallpapers) {
    listImage = wallpapers;
    if (listImage != null && listImage.size() > 0) {
      homeBinding.wallpapers.setVisibility(View.VISIBLE);
      homeBinding.noData.getRoot().setVisibility(View.GONE);
      mPhotoAdapter = new PhotoAdapter(getActivity(), listImage, isConnected(), this);
      GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
      gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
        @Override
        public int getSpanSize(int position) {
          return mPhotoAdapter.getItemViewType(position) == 1 ?  3 : 1;
        }
      });

      homeBinding.wallpapers.setLayoutManager(gridLayoutManager);
      homeBinding.wallpapers.setItemAnimator(new DefaultItemAnimator());
      homeBinding.wallpapers.setAdapter(mPhotoAdapter);
      mPhotoAdapter.notifyDataSetChanged();
    } else {
      homeBinding.wallpapers.setVisibility(View.GONE);
      homeBinding.noData.getRoot().setVisibility(View.VISIBLE);
    }
  }

  private List<Wallpaper> loadData()  {
    List<Wallpaper> list = new ArrayList<>();
    DatabaseReference ref2 =  mDatabase.child(Config.WALLPAPER);
    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
          Wallpaper wallpaper = dsp.getValue(Wallpaper.class);
          wallpaper.setFavorite(db.isFavoriteWallpaper(wallpaper.getId()) ? 1: 0);
          if (!db.isWallpaper(wallpaper.getId())) {
            db.addWallpaper(wallpaper);
          } else {
            db.updateWallpaper(wallpaper);
          }
          list.add(wallpaper);
        }
        Collections.reverse(list);
        adapter(list);

      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
        listImage = db.listWallpaper();
        Collections.reverse(listImage);
        if (listImage.size() > 0) {
          adapter(listImage);
        }
      }
    });

    return list;
  }


  @Override
  public void selectedPosition(int position) {
    if (!isConnected()) {
      Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
      return;
    }
    Intent intent = new Intent(getActivity(), SetWallpaper.class);
    intent.putExtra("wallpaper", listImage.get(position));
    startActivity(intent);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == Config.REQUEST_CODE_REFRESH) {
      listImage = db.listWallpaper();
      Collections.reverse(listImage);
      adapter(listImage);
    }

  }

}