package com.anhttvn.lionelmessiwallpaper.ui.recent;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.lionelmessiwallpaper.adapter.GalleryAdapter;
import com.anhttvn.lionelmessiwallpaper.adapter.PhotoAdapter;
import com.anhttvn.lionelmessiwallpaper.database.ConfigData;
import com.anhttvn.lionelmessiwallpaper.databinding.FragmentRecentBinding;
import com.anhttvn.lionelmessiwallpaper.layout.SetWallpaper;
import com.anhttvn.lionelmessiwallpaper.layout.SettingWallpaper;
import com.anhttvn.lionelmessiwallpaper.model.Wallpaper;
import com.anhttvn.lionelmessiwallpaper.util.BaseFragment;
import com.anhttvn.lionelmessiwallpaper.util.Config;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author  anhtt61
 * @version 1.1.3
 * @2022
 */
public class RecentFragment extends BaseFragment implements GalleryAdapter.EventGallery {
  private FragmentRecentBinding recentBinding;
  private List<String> wallpapers = new ArrayList<>();
  private GalleryAdapter adapter;

  @Override
  protected View initView(LayoutInflater inflater, ViewGroup container, boolean b) {
    recentBinding = FragmentRecentBinding.inflate(inflater, container, b);
    return recentBinding.getRoot();
  }

  @Override
  protected void init() {
    adapter(images("wallpaper"));
  }


  private void adapter(List<String> list) {
    wallpapers = list;
    if (wallpapers != null && wallpapers.size() > 0) {
      recentBinding.listRecent.setVisibility(View.VISIBLE);
      recentBinding.noData.getRoot().setVisibility(View.GONE);
      adapter = new GalleryAdapter(getActivity(), wallpapers, "Gallery" ,this);
      RecyclerView.LayoutManager layoutManager =
              new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
      recentBinding.listRecent.setLayoutManager(layoutManager);
      recentBinding.listRecent.setItemAnimator(new DefaultItemAnimator());
      recentBinding.listRecent.setAdapter(adapter);
      adapter.notifyDataSetChanged();
    } else {
      recentBinding.listRecent.setVisibility(View.GONE);
      recentBinding.noData.getRoot().setVisibility(View.VISIBLE);
    }
    recentBinding.progressBar.setVisibility(View.GONE);
  }

  public  ArrayList<String> images(String folderPath) {
    ArrayList<String> pathList = new ArrayList<>();
    try {
      String[] files = getContext().getAssets().list(folderPath);
      for (String name : files) {
        pathList.add(folderPath + File.separator + name);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return pathList;
  }
  @Override
  public void sendWallpaper(int position) {
    Intent intent = new Intent(getActivity(), SettingWallpaper.class);
    intent.putExtra("wallpaper", wallpapers.get(position));
    intent.putExtra("type", "Gallery");
    startActivity(intent);
  }
}
