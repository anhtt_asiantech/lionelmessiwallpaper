package com.anhttvn.lionelmessiwallpaper.database;

public class ConfigData {
  public static final String DATABASE = "LionelMessiWallpaper.db";
  public static final int VERSION = 2;
  public static final String TABLE = "Wallpaper";

  // detail table
  public static final String TITLE = "title";
  public static final String PATH = "path";
  public static final String FAVORITE ="Favorite";
  public static final String VIEW ="view";
  public static final String ID ="id";
  public static final String ADS ="ads";

  // create table
  public static final String CREATE_TABLE =
          "CREATE TABLE " + TABLE + " (" +
                  ID + " VARCHAR(100) PRIMARY KEY," +
                  TITLE + " VARCHAR(1000)," +
                  PATH +" VARCHAR(1000)," +
                  FAVORITE + " INTEGER, " +
                  VIEW +" INTEGER, " +
                  ADS +" INTEGER)";


  //delete table
  public static final String DELETE_TABLE =
          "DROP TABLE IF EXISTS " + TABLE;
}
