package com.anhttvn.lionelmessiwallpaper.layout;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.anhttvn.lionelmessiwallpaper.R;
import com.anhttvn.lionelmessiwallpaper.databinding.ActivityGralleryWallpaperBinding;
import com.anhttvn.lionelmessiwallpaper.util.BaseActivity;
import com.google.android.gms.ads.AdView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SettingWallpaper extends BaseActivity {
  private ActivityGralleryWallpaperBinding wallpaperBinding;
  private boolean isAllVisibleFab = false;
  private String path;
  private Bitmap bitmap;

  @Override
  public void init() {
    getSupportActionBar().hide();
    wallpaperBinding.tab.homeWall.setVisibility(View.GONE);
    wallpaperBinding.tab.lockWall.setVisibility(View.GONE);
    wallpaperBinding.tab.favorite.setVisibility(View.GONE);
    wallpaperBinding.tab.favorite.setImageResource(R.drawable.icon_share);
    wallpaperBinding.tab.download.setImageResource(R.drawable.ic_back);
    wallpaperBinding.tab.download.setVisibility(View.GONE);
    wallpaperBinding.tab.bottom.setVisibility(View.GONE);
    isBannerADS(wallpaperBinding.ads);
    loadData();
    evenFunction();
  }

  @Override
  public View contentView() {
    wallpaperBinding = ActivityGralleryWallpaperBinding.inflate(getLayoutInflater());
    return wallpaperBinding.getRoot();
  }

  private void loadData() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    path = (String)bundle.getSerializable("wallpaper");
    String type = (String)bundle.getSerializable("type");

    if (path == null || path.isEmpty()) {
      return;
    }
    if (type.compareToIgnoreCase("Gallery") == 0) {
      InputStream inputstream= null;
      try {
        inputstream = getApplicationContext().getAssets().open(path);
      } catch (IOException e) {
        e.printStackTrace();
      }
      Drawable drawable = Drawable.createFromStream(inputstream, null);
      bitmap = drawableToBitmap(drawable);
      wallpaperBinding.imgWallpaper.setImageDrawable(drawable);
    } else {
      File imgFile = new File(path);
      if(imgFile.exists())
      {
        bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        wallpaperBinding.imgWallpaper.setImageBitmap(bitmap);
      }
    }

  }

  private void evenFunction() {
    wallpaperBinding.tab.menu.setOnClickListener(v -> {
      if (!isAllVisibleFab) {
        wallpaperBinding.tab.menu.setImageResource(R.drawable.ic_close);
        wallpaperBinding.tab.homeWall.show();
        wallpaperBinding.tab.lockWall.show();
        wallpaperBinding.tab.favorite.show();
        wallpaperBinding.tab.download.show();
        isAllVisibleFab = true;
      } else {
        wallpaperBinding.tab.menu.setImageResource(R.drawable.ic_menu);
        wallpaperBinding.tab.homeWall.hide();
        wallpaperBinding.tab.lockWall.hide();
        wallpaperBinding.tab.favorite.hide();
        wallpaperBinding.tab.download.hide();
        isAllVisibleFab = false;
      }
    });

    wallpaperBinding.tab.homeWall.setOnClickListener(v -> {
      if (bitmap == null) {
        return;
      }
      isQuestion(getString(R.string.home_screen), getString(R.string.question_set_home), "Home");

    });

    wallpaperBinding.tab.lockWall.setOnClickListener(v -> {
      if (bitmap == null) {
        return;
      }
      isQuestion(getString(R.string.lock_screen), getString(R.string.question_set_lock), "Lock");

    });

    wallpaperBinding.tab.download.setOnClickListener(v -> {
      isADSFull();
      finish();
    });

    wallpaperBinding.tab.favorite.setOnClickListener(v -> {
      shareImage(bitmap);
    });

  }

  public Bitmap drawableToBitmap(Drawable drawable) {
    if (drawable instanceof BitmapDrawable) {
      return ((BitmapDrawable) drawable).getBitmap();
    }

    // We ask for the bounds if they have been set as they would be most
    // correct, then we check we are  > 0
    final int width = !drawable.getBounds().isEmpty() ?
            drawable.getBounds().width() : drawable.getIntrinsicWidth();

    final int height = !drawable.getBounds().isEmpty() ?
            drawable.getBounds().height() : drawable.getIntrinsicHeight();

    // Now we check we are > 0
    final Bitmap bitmap = Bitmap.createBitmap(width <= 0 ? 1 : width, height <= 0 ? 1 : height,
            Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
    drawable.draw(canvas);

    return bitmap;
  }

  private void shareImage(Bitmap bitmap) {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

    String path = MediaStore.Images.Media.insertImage(this.getContentResolver(), bitmap, getString(R.string.app_name), null);
    Uri.parse(path);

    Intent i = new Intent(Intent.ACTION_SEND);

    i.setType("image/*");
    i.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
    try {
      startActivity(Intent.createChooser(i, getString(R.string.share_image)));
    } catch (android.content.ActivityNotFoundException ex) {
      ex.printStackTrace();
    }
  }

  private void isQuestion(String title, String message, String type) {
    AlertDialog.Builder dialogBuilder =	new AlertDialog.Builder(this);
    LayoutInflater inflater	= this.getLayoutInflater();
    View dialogView	= inflater.inflate(R.layout.layout_exits, null);
    TextView tvTitle = dialogView.findViewById(R.id.exit);
    tvTitle.setText(title);
    TextView tvMessage = dialogView.findViewById(R.id.message);
    tvMessage.setText(message);
    dialogBuilder.setView(dialogView);
    AdView ads = dialogView.findViewById(R.id.ads);
    ads.setVisibility(View.GONE);
//    if (isConnected()) {
//      ads.setVisibility(View.VISIBLE);
//      isBannerADS(ads);
//    }
    AlertDialog b = dialogBuilder.create();
    dialogView.findViewById(R.id.btnNo).setOnClickListener(v -> {
      b.dismiss();
    });
    dialogView.findViewById(R.id.btnYes).setOnClickListener(v -> {
      b.dismiss();
      if (type.equalsIgnoreCase("Home")) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.please_set_wallpaper));
        dialog.show();

        new Handler().postDelayed(
                () -> {
                  homeWall(bitmap);
                  dialog.dismiss();
                },
                1000);
      } else {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.please_set_wallpaper));
        dialog.show();

        new Handler().postDelayed(
                () -> {
                  lockWall(bitmap);
                  dialog.dismiss();
                },
                1000);
      }
    });
    b.show();
  }
}
