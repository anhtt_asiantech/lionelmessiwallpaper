package com.anhttvn.lionelmessiwallpaper.layout;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.anhttvn.lionelmessiwallpaper.R;
import com.anhttvn.lionelmessiwallpaper.databinding.ActivitySetWallpaperBinding;
import com.anhttvn.lionelmessiwallpaper.model.MessageEvent;
import com.anhttvn.lionelmessiwallpaper.model.Wallpaper;
import com.anhttvn.lionelmessiwallpaper.util.BaseActivity;
import com.anhttvn.lionelmessiwallpaper.util.Config;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;

/**
 * @author anhtt61
 * @version 1.1.3
 * @2022
 */
public class SetWallpaper extends BaseActivity {
  private ActivitySetWallpaperBinding setWallpaperBinding;
  private Wallpaper wallpaper;
  private boolean isAllVisibleFab = false;
  ProgressDialog dialog;
  private DatabaseReference mDatabase;


  @Override
  public void init() {
    getSupportActionBar().hide();
    setWallpaperBinding.fab.menu.setVisibility(View.GONE);
    setWallpaperBinding.fab.homeWall.setVisibility(View.GONE);
    setWallpaperBinding.fab.lockWall.setVisibility(View.GONE);
    setWallpaperBinding.fab.favorite.setVisibility(View.GONE);
    setWallpaperBinding.fab.download.setVisibility(View.GONE);
    setWallpaperBinding.fab.bottom.setVisibility(View.GONE);
    mDatabase = FirebaseDatabase.getInstance().getReference(Config.WALLPAPER);

    isBannerADS(setWallpaperBinding.ads);
    getDataIntent();
    eventSetWallpaper();
    changeIconFavorite(db.isFavoriteWallpaper(wallpaper.getId()));
  }


  private void eventSetWallpaper() {
    setWallpaperBinding.fab.menu.setOnClickListener(v -> {
      isMenuDisable();
    });

    setWallpaperBinding.fab.homeWall.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      isQuestion(getString(R.string.home_screen), getString(R.string.question_set_home), "Home");
    });
    setWallpaperBinding.fab.lockWall.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      isQuestion(getString(R.string.lock_screen), getString(R.string.question_set_lock), "Lock");
    });

    setWallpaperBinding.fab.download.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      downloadImage(wallpaper.getPath(), wallpaper.getTitle());
    });

    setWallpaperBinding.fab.favorite.setOnClickListener(v -> {
      changeFavorite();
    });
    setWallpaperBinding.fab.bottom.setOnClickListener(v -> {
      EventBus.getDefault().postSticky(new MessageEvent("UPDATE"));
      isADSFull();
      finish();
    });
  }

  private void changeIconFavorite(boolean is) {
    setWallpaperBinding.fab.favorite.setImageResource(is ? R.drawable.ic_favorite_disabel : R.drawable.icon_favorite_disable);
  }

  private void changeFavorite() {
    boolean isExits = db.isFavoriteWallpaper(wallpaper.getId());
    if (isExits) {
      changeIconFavorite(false);
      wallpaper.setFavorite(0);

    } else {
      changeIconFavorite(true);
      wallpaper.setFavorite(1);
    }
    db.updateWallpaper(wallpaper);
  }


  @Override
  public View contentView() {
    setWallpaperBinding = ActivitySetWallpaperBinding.inflate(getLayoutInflater());
    return setWallpaperBinding.getRoot();
  }

  private void getDataIntent() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    ProgressDialog loadWallpaper = new ProgressDialog(this);
    loadWallpaper.setMessage(getString(R.string.loadWallpaper));
    loadWallpaper.show();
    wallpaper = (Wallpaper) bundle.getSerializable("wallpaper");
    if (wallpaper != null) {
      updateView(wallpaper);
      Picasso.with(getApplicationContext()).load(wallpaper.getPath())
              .placeholder(R.drawable.ic_no_thumbnail)
              .error(R.drawable.ic_no_thumbnail)
              .into(setWallpaperBinding.imgWallpaper, new Callback() {
                @Override
                public void onSuccess() {
                  setWallpaperBinding.fab.menu.setVisibility(View.VISIBLE);
                  loadWallpaper.dismiss();
                }

                @Override
                public void onError() {
                  loadWallpaper.dismiss();
                  showToast(getString(R.string.notLoadImage));
                }
              });
    }

  }

  private void setWallpaper(String type) {
    dialog = new ProgressDialog(this);
    dialog.setMessage(getString(R.string.please_set_wallpaper));
    dialog.show();
    Picasso.with(this)
            .load(wallpaper.getPath())
            .into(new Target() {
              @Override
              public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                if (type.equalsIgnoreCase("Home")) {
                  homeWall(bitmap);
                } else {
                  lockWall(bitmap);
                }
                isMenuDisable();
                dialog.dismiss();
              }

              @Override
              public void onBitmapFailed(Drawable errorDrawable) {
                dialog.dismiss();
              }

              @Override
              public void onPrepareLoad(Drawable placeHolderDrawable) {

              }
            });
  }

  private void isMenuDisable() {
    if (!isAllVisibleFab) {
      setWallpaperBinding.fab.menu.setImageResource(R.drawable.ic_close);
      setWallpaperBinding.fab.homeWall.show();
      setWallpaperBinding.fab.lockWall.show();
      setWallpaperBinding.fab.favorite.show();
      setWallpaperBinding.fab.download.show();
      setWallpaperBinding.fab.bottom.show();
      isAllVisibleFab = true;
    } else {
      setWallpaperBinding.fab.menu.setImageResource(R.drawable.ic_menu);
      setWallpaperBinding.fab.homeWall.hide();
      setWallpaperBinding.fab.lockWall.hide();
      setWallpaperBinding.fab.favorite.hide();
      setWallpaperBinding.fab.download.hide();
      setWallpaperBinding.fab.bottom.hide();
      isAllVisibleFab = false;
    }
  }

  private void isQuestion(String title, String message, String type) {
    AlertDialog.Builder dialogBuilder =	new AlertDialog.Builder(this);
    LayoutInflater inflater	= this.getLayoutInflater();
    View dialogView	= inflater.inflate(R.layout.layout_exits, null);
    TextView tvTitle = dialogView.findViewById(R.id.exit);
    tvTitle.setText(title);
    TextView tvMessage = dialogView.findViewById(R.id.message);
    tvMessage.setText(message);
    dialogBuilder.setView(dialogView);
    AdView ads = dialogView.findViewById(R.id.ads);
    ads.setVisibility(View.GONE);
    if (isConnected()) {
      ads.setVisibility(View.VISIBLE);
      isBannerADS(ads);
    }
    AlertDialog b = dialogBuilder.create();
    dialogView.findViewById(R.id.btnNo).setOnClickListener(v -> {
      b.dismiss();
    });
    dialogView.findViewById(R.id.btnYes).setOnClickListener(v -> {
      b.dismiss();
      setWallpaper(type);
    });
    b.show();
  }

  private void updateView(Wallpaper wallpaper) {

    mDatabase.child(wallpaper.getId()).get().addOnCompleteListener(task -> {
      if (task.isSuccessful()) {
        Wallpaper wallpaperUpdate = task.getResult().getValue(Wallpaper.class);

        int viewNumber = wallpaperUpdate.getView() + 1;

        wallpaper.setView(viewNumber);
        mDatabase.child(wallpaperUpdate.getId()).child("view").setValue(wallpaper.getView());
        db.updateWallpaper(wallpaper);
      }
      else {
        Log.e("firebase", "Error getting data", task.getException());

      }
    });

  }

  @Override
  public void onBackPressed() {
    EventBus.getDefault().postSticky(new MessageEvent("UPDATE"));
    isADSFull();
    finish();
    super.onBackPressed();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if(dialog != null && dialog.isShowing()) {
      dialog.dismiss();
      dialog = null;
    }
  }
}
