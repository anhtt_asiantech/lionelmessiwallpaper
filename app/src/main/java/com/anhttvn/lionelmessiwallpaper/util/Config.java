package com.anhttvn.lionelmessiwallpaper.util;

public class Config {
  public static final String FOLDER_DOWNLOAD ="LionelMessiWallpaper";
  public static final String WALLPAPER ="Wallpaper_v2";
  public static final String BANNER ="Banner";

  public static final String URL_APP = "https://play.google.com/store/apps/details?id=com.anhttvn.lionelmessiwallpaper";
  public  static final int PERMISSION_REQUEST_CODE = 7;

  public static final int REQUEST_CODE_REFRESH = 10;

}
