package com.anhttvn.lionelmessiwallpaper.model;

public class Banner {
  public String id;
  public String path;
  public boolean ads;
  public int version;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public boolean isAds() {
    return ads;
  }

  public void setAds(boolean ads) {
    this.ads = ads;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }


}
