package com.anhttvn.lionelmessiwallpaper;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;

import com.anhttvn.lionelmessiwallpaper.databinding.ActivityMainBinding;
import com.anhttvn.lionelmessiwallpaper.layout.PrivatePolice;
import com.anhttvn.lionelmessiwallpaper.model.MessageEvent;
import com.anhttvn.lionelmessiwallpaper.ui.download.DownloadFragment;
import com.anhttvn.lionelmessiwallpaper.ui.favorite.FavoriteFragment;
import com.anhttvn.lionelmessiwallpaper.ui.home.HomeFragment;
import com.anhttvn.lionelmessiwallpaper.ui.recent.RecentFragment;
import com.anhttvn.lionelmessiwallpaper.util.BaseActivity;
import com.anhttvn.lionelmessiwallpaper.util.Config;
import com.google.android.material.navigation.NavigationView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @author anhtt61
 * @version 1.1.3
 */

public class Main extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

  private ActivityMainBinding mainBinding;


  @Override
  public void init() {
    setSupportActionBar(mainBinding.header.toolbar);
    getSupportActionBar().setTitle(isConnected() ? getString(R.string.wallpaperOnline) : getString(R.string.wallpaperOffline));
    mainBinding.navView.setNavigationItemSelectedListener(this);

    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mainBinding.drawerLayout, mainBinding.header.toolbar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    mainBinding.drawerLayout.addDrawerListener(toggle);
    toggle.syncState();
    if (savedInstanceState == null && isConnected()) {
      getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
              new HomeFragment()).commit();
      mainBinding.navView.setCheckedItem(R.id.home);

    }

    if (!isConnected()) {
      mainBinding.navView.getMenu().findItem(R.id.home).setVisible(false);
      mainBinding.navView.getMenu().findItem(R.id.favorite).setVisible(false);
      mainBinding.navView.getMenu().findItem(R.id.gallery).setVisible(false);

      mainBinding.navView.setCheckedItem(R.id.recent);
      getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
              new RecentFragment()).commit();
      mainBinding.navView.setCheckedItem(R.id.recent);
    }

  }

  @Override
  public View contentView() {
    mainBinding = ActivityMainBinding.inflate(getLayoutInflater());
    return mainBinding.getRoot();
  }

  @SuppressLint("NonConstantResourceId")
  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    switch (item.getItemId()) {
      case R.id.home:
        mainBinding.header.toolbar.setTitle(getString(R.string.wallpaperOnline));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new HomeFragment()).commit();
        break;
      case R.id.recent:
        mainBinding.header.toolbar.setTitle(getString(R.string.wallpaperOffline));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new RecentFragment()).commit();
        break;
      case R.id.favorite:
        mainBinding.header.toolbar.setTitle(getString(R.string.favorite));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new FavoriteFragment()).commit();
        break;
      case R.id.gallery:
        mainBinding.header.toolbar.setTitle(getString(R.string.download));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new DownloadFragment()).commit();
        break;

//      case R.id.share:
//        Intent i = new Intent(Intent.ACTION_SEND);
//        i.setType("text/plain");
//        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_app));
//        i.putExtra(Intent.EXTRA_TEXT, Config.URL_APP);
//        startActivity(Intent.createChooser(i, "Share"));
//        break;
//      case R.id.rate:
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                Uri.parse(Config.URL_APP));
//        startActivity(browserIntent);
//        break;
      case R.id.privacyPolice:
        Intent intent = new Intent(this, PrivatePolice.class);
        startActivity(intent);
        break;
      default:
        break;
    }
    mainBinding.drawerLayout.closeDrawer(GravityCompat.START);
    return true;
  }


  @Override
  public void onBackPressed() {
    showExit();
  }

  private void showExit() {
    AlertDialog.Builder dialogBuilder =	new AlertDialog.Builder(this);
    LayoutInflater inflater	= this.getLayoutInflater();
    View dialogView	= inflater.inflate(R.layout.layout_exits, null);
    isBannerADS(dialogView.findViewById(R.id.ads));
    dialogBuilder.setView(dialogView);
    AlertDialog b = dialogBuilder.create();

    dialogView.findViewById(R.id.btnNo).setOnClickListener(v -> b.dismiss());
    dialogView.findViewById(R.id.btnYes).setOnClickListener(v -> {
      b.dismiss();
      finish();
    });
    b.show();

  }
  @Override
  protected void onDestroy() {
    super.onDestroy();

  }

  @Override
  protected void onStart() {
    super.onStart();
    EventBus.getDefault().register(this);
  }

  @Override
  protected void onStop() {
    super.onStop();
    EventBus.getDefault().unregister(this);
  }

  @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
  public void onMessageEvent(MessageEvent event) {
    try {
      if (event.action.equalsIgnoreCase("UPDATE")) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
          if (fragment instanceof HomeFragment) {
            fragment.onActivityResult(0, Config.REQUEST_CODE_REFRESH, null);

          }

        }
      }
    }catch (Exception e) {
      showToast("main : " +e );
    }

  }
}